const { app, BrowserWindow, ipcMain} = require('electron')
const log = require('electron-log')
const { autoUpdater } = require('electron-updater')
const path = require('path')
const {is, isFirstAppLaunch} = require('electron-util')
const fs = require('fs')
const db = require('electron-db')

console.log(app.getPath('userData'))

app.disableHardwareAcceleration();
app.setAppUserModelId(process.execPath);

if (process.mas) app.setName('Trainz Config');

autoUpdater.logger = log;
autoUpdater.logger.transports.file.level = 'info';
log.info('App starting...');


let main;
let pathJob = path.join(`${__dirname}`, 'job')
let pathDatabase = path.join(`${__dirname}`, 'database')

let dir = path.join(__dirname, 'app', 'assets', 'js', 'loading.js')
console.log(pathJob)

function createWindow() {
    main = new BrowserWindow({
        width: 1250,
        height: 830,
        icon: getPlatformIcon('configtrainz'),
        frame: false,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true,
            worldSafeExecuteJavaScript: true,
        },
        backgroundColor: '#171614',
        show: false
    });

    main.on('close', () => {
        main = null;
    });

    main.removeMenu()
    main.resizable = true

    if(is.development === true) {
        main.webContents.openDevTools();
        main.maximize();
    }

    main.loadURL(`file://${__dirname}/app/html/index.html`).then(r => {
        return main
    })

    main.once('ready-to-show', () => {
        autoUpdater.checkForUpdatesAndNotify();

    })



    return main;
}

function getPlatformIcon(filename){
    const opSys = process.platform;
    if (opSys === 'darwin') {
        filename = filename + '.icns'
    } else if (opSys === 'win32') {
        filename = filename + '.ico'
    } else {
        filename = filename + '.png'
    }

    return path.join(__dirname, 'app', 'assets', 'images', filename)
}

function clearCacheFile() {
    try {
        fs.accessSync(`${app.getPath('userData')}/logs/main.log`, fs.constants.F_OK)
        fs.unlinkSync(`${app.getPath('userData')}/logs/main.log`)
    }catch (e) {
        log.info("Main Log doesn't exist !")
    }

    try {
        fs.accessSync(`${app.getPath('userData')}/logs/renderer.log`, fs.constants.F_OK)
        fs.unlinkSync(`${app.getPath('userData')}/logs/renderer.log`)
    }catch (e) {
        log.info("Renderer Log doesn't exist !")
    }
}

function createPath() {
    fs.mkdirSync(`${pathJob}/imports`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/animaux`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/autorail_diesel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/autorail_electrique`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/batiment`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/decors`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/gare_fonctionnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/gare_non_fonctionnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/html`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/industrie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/locomotive_diesel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/locomotive_electrique`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/locomotive_vapeur`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/marchandise`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/objet_de_gare`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/objet_de_voie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/pont`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/regle`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/tunnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/voie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/voiture_marchandise`, {recursive: true})
    fs.mkdirSync(`${pathJob}/imports/voiture_voyageur`, {recursive: true})

    fs.mkdirSync(`${pathJob}/scenes`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/animaux`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/autorail_diesel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/autorail_electrique`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/batiment`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/decors`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/gare_fonctionnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/gare_non_fonctionnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/html`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/industrie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/locomotive_diesel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/locomotive_electrique`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/locomotive_vapeur`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/marchandise`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/objet_de_gare`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/objet_de_voie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/pont`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/regle`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/tunnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/voie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/voiture_marchandise`, {recursive: true})
    fs.mkdirSync(`${pathJob}/scenes/voiture_voyageur`, {recursive: true})

    fs.mkdirSync(`${pathJob}/exports`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/animaux`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/autorail_diesel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/autorail_electrique`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/batiment`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/decors`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/gare_fonctionnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/gare_non_fonctionnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/html`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/industrie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/locomotive_diesel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/locomotive_electrique`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/locomotive_vapeur`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/marchandise`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/objet_de_gare`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/objet_de_voie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/pont`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/regle`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/tunnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/voie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/voiture_marchandise`, {recursive: true})
    fs.mkdirSync(`${pathJob}/exports/voiture_voyageur`, {recursive: true})

    fs.mkdirSync(`${pathJob}/publish`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/animaux`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/autorail_diesel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/autorail_electrique`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/batiment`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/decors`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/gare_fonctionnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/gare_non_fonctionnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/html`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/industrie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/locomotive_diesel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/locomotive_electrique`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/locomotive_vapeur`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/marchandise`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/objet_de_gare`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/objet_de_voie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/pont`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/regle`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/tunnel`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/voie`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/voiture_marchandise`, {recursive: true})
    fs.mkdirSync(`${pathJob}/publish/voiture_voyageur`, {recursive: true})

    fs.mkdirSync(`${pathDatabase}`, {recursive: true})
}

function createDatabase() {
    db.createTable('category', pathDatabase, (succ, msg) => {
        if(!succ) throw console.error(msg)
    })

    db.createTable('assets', pathDatabase, (succ, msg) => {
        if(!succ) throw console.error(msg)
    })

    db.createTable('configs', pathDatabase, (succ, msg) => {
        if(!succ) throw console.error(msg)
    })
}

function insertValueConfig() {
    let Obj = {
        trainzPath: null,
        software3d: null,
        painter3d: null,
        identifier: 1,
        trainz_identifier: null,
    }

    if(db.valid('configs', pathDatabase)) {
        db.insertTableContent('configs', pathDatabase ,Obj, (succ, err) => {
            if(!succ) throw console.log(err)
        })
    }
}

function insertValueCategory() {
    let arr = [
        {
            id: 1,
            name: 'Animaux',
            slug: 'animaux'
        },
        {
            id: 2,
            name: 'Autorail Diesel',
            slug: 'autorail_diesel'
        },
        {
            id: 3,
            name: 'Autorail Electrique',
            slug: 'autorail_electrique'
        },
        {
            id: 4,
            name: 'Batiment',
            slug: 'batiment'
        },
        {
            id: 5,
            name: 'Decors',
            slug: 'decors'
        },
        {
            id: 6,
            name: 'Gare Fonctionnel',
            slug: 'gare_fonctionnel'
        },
        {
            id: 7,
            name: 'Gare Non Fonctionnel',
            slug: 'gare_non_fonctionnel'
        },
        {
            id: 8,
            name: 'Html',
            slug: 'html'
        },
        {
            id: 9,
            name: 'Industrie',
            slug: 'industrie'
        },
        {
            id: 10,
            name: 'Locomotive Diesel',
            slug: 'locomotive_diesel'
        },
        {
            id: 11,
            name: 'Locomotive Electrique',
            slug: 'locomotive_electrique'
        },
        {
            id: 12,
            name: 'Locomotive Vapeur',
            slug: 'locomotive_vapeur'
        },
        {
            id: 13,
            name: 'Marchandise',
            slug: 'marchandise'
        },
        {
            id: 14,
            name: 'Objet de Gare',
            slug: 'objet_de_gare'
        },
        {
            id: 15,
            name: 'Objet de Voie',
            slug: 'objet_de_voie'
        },
        {
            id: 16,
            name: 'Pont',
            slug: 'pont'
        },
        {
            id: 17,
            name: 'Règle',
            slug: 'regle'
        },
        {
            id: 18,
            name: 'Tunnel',
            slug: 'tunnel'
        },
        {
            id: 19,
            name: 'Voie',
            slug: 'voie'
        },
        {
            id: 20,
            name: 'Voiture Marchandise',
            slug: 'voiture_marchandise'
        },
        {
            id: 21,
            name: 'Voiture Voyageur',
            slug: 'voiture_voyageur'
        },
    ]

    Array.from(arr).forEach((item) => {
        let obj = {
            identifier: item.id,
            name: item.name,
            path: item.slug
        }

        if(db.valid('category', pathDatabase)) {
            db.insertTableContent('category', pathDatabase, obj, (succ, err) => {
                if(!succ) throw console.log(err)
            })
        }
    })
}

app.on('ready', () => {
    if(isFirstAppLaunch === true || !fs.existsSync(pathDatabase + '/category.json') || !fs.existsSync(pathDatabase + '/assets.json')) {
        createPath()
        createDatabase()
        insertValueConfig()
        insertValueCategory()
        createWindow()
    } else {
        clearCacheFile()
        createWindow()
    }
})

app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
        createWindow()
    }
});

ipcMain.on('loading-terminate', (event, arg) => {
    if(arg === 'ok') {
        createWindow()
    } else {

    }
})

autoUpdater.on('update-available', () => {
    log.info("Mise à jour disponible")
    win.webContents.send('update_available');
});
autoUpdater.on('update-downloaded', () => {
    log.info('Mise à jour télécharger')
    win.webContents.send('update_downloaded');
});