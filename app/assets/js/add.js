const {BrowserWindow} = require('electron').remote
const path = require('path')
const db = require('electron-db');
const log = require('electron-log')
const fs = require('fs')
const Utils = require('../assets/js/Utils/Utils.js')
const utils = new Utils()
const slugify = require('slugify')

let pathDatabase = path.join(`${__dirname}`, '..', '..', 'database')
let pathJob = path.join(`${__dirname}`, '..', '..', 'job')
let config;
let counter;
const ElementsAdd = {
    categorySelector: utils.getId('categorySelector')
}

console.log(pathDatabase)

function notificator(title, body) {
    const notification = {
        title: title,
        body: body
    }

    new window.Notification(notification.title, notification)
}

function loadConfig() {
    db.getRows('configs', pathDatabase, {"identifier": 1}, (succ, result) => {
        config = result[0]
    })
}

function loadCategories() {
    db.getAll('category', pathDatabase, (succ, data) => {
        ElementsAdd.categorySelector.innerHTML = `<option value="">Tous</option>`;
        Array.from(data).forEach((item) => {
            ElementsAdd.categorySelector.innerHTML += `<option value="${item.path}">${item.name}</option>`;
        })
    })
}

function countAssets() {
    db.getAll('assets', pathDatabase, (succ, data) => {
        counter = data.length
    })
}

function getKindByCategory(category) {
    switch (category) {
        case 'animaux': return 'scenery';
        case 'autorail_diesel': return 'traincar';
        case 'autorail_electrique': return 'traincar';
        case 'batiment': return 'scenery';
        case 'decors': return 'scenery';
        case 'gare_fonctionnel': return 'industry';
        case 'gare_non_fonctionnel': return 'scenery';
        case 'html': return 'html-asset';
        case 'industrie': return 'industry';
        case 'locomotive_diesel': return 'traincar';
        case 'locomotive_electrique': return 'traincar';
        case 'locomotive_vapeur': return 'traincar';
        case 'marchandise': return 'product';
        case 'objet_de_gare': return 'scenery';
        case 'pont': return 'bridge';
        case 'regle': return 'behavior';
        case 'tunnel': return 'bridge';
        case 'voie': return 'track';
        case 'voiture_marchandise': return 'traincar';
        case 'voiture_voyageur': return 'traincar';
    }
}


$("#formAddObject").on('submit', (e) => {
    e.preventDefault()
    let form = $("#formAddObject")
    let btn = form.find('.btn-success')

    btn.attr('disabled', true).html('<i class="fa fa-spinner fa-spin"></i> Chargement')

    let obj = {};
    obj.identifier = (counter === 0) ? 1 : counter++
    obj.designation = utils.getId('username').value
    obj.state = 'draft'
    obj.category = utils.getId('categorySelector').value
    if(utils.getId('kuidNumberVersion').value !== '') {
        obj.kuid = `kuid2:${config.trainz_identifier}:${utils.getId('kuidNumber').value}:${utils.getId('kuidNumberVersion').value}`
    } else {
        obj.kuid = `kuid:${config.trainz_identifier}:${utils.getId('kuidNumber').value}`
    }
    if(utils.getId('lodDistance').value !== ''){obj.lodDistance = utils.getId('lodDistance').value }else{ obj.lodDistance = ''}

    if(db.valid('assets', pathDatabase)) {
        db.insertTableContent('assets', pathDatabase, obj, (succ, msg) => {
            notificator(obj.designation, "Cette objet à été créer !")
        })
    }

    // Création du dossier d'import
    fs.mkdir(`${pathJob}/imports/${obj.category}/${slugify(obj.designation)}`, (err) => {
        if(err) throw log.error("Create Import", err);
    })

    // Création du dossier scenes
    fs.mkdir(`${pathJob}/scenes/${obj.category}/${slugify(obj.designation)}`, (err) => {
        if(err) throw log.error("Create Scenes", err);
    })

    // Création du dossier d'export
    fs.mkdir(`${pathJob}/exports/${obj.category}/${slugify(obj.designation)}`, (err) => {
        if(err) throw log.error("Create Exports", err);
    })

    // Création du dossier publish
    fs.mkdir(`${pathJob}/publish/${obj.category}/${slugify(obj.designation)}`, (err) => {
        if(err) throw log.error("Create Publish", err);

        setTimeout(() => {
            BrowserWindow.getFocusedWindow().close()
        }, 1000);
    })

})


loadCategories()
loadConfig()
countAssets()


