const {BrowserWindow} = require('electron').remote
const Utils = require('../assets/js/Utils/Utils.js')
const utils = new Utils()
const {is} = require('electron-util')
const path = require('path')

let Elements = {
    moduleElem: utils.getId('loadModule'),

    btnClose: utils.getId('close'),
    headClose: utils.getId('headClose'),
    minimize:utils.getId('minimize'),
    maximize: utils.getId('maximize'),

    menuNews: utils.getId('menuNews'),
    createObject: utils.getId('createObject')
};

Elements.btnClose.addEventListener('click', () => {
    BrowserWindow.getFocusedWindow().close()
})

Elements.headClose.addEventListener('click', () => {
    BrowserWindow.getFocusedWindow().close()
})

Elements.minimize.addEventListener('click', () => {
    BrowserWindow.getFocusedWindow().minimize();
})

Elements.maximize.addEventListener('click', () => {
    BrowserWindow.getFocusedWindow().maximize();
})

Elements.createObject.addEventListener('click', () => {
    createWindowAddObject()
})

function createWindowAddObject() {
    let add = new BrowserWindow(Object.assign({
        width:1250,
        height: 830,
        frame: false,
        transparent: true,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true,
            worldSafeExecuteJavaScript: true
        },
    }))

    add.loadURL(path.join(`${__dirname}`, 'add.html')).then(r => {
        return add
    })

    if(is.development === true) {
        add.webContents.openDevTools();
        add.maximize();
    }

    add.webContents.on('did-finish-load', () => {
        add.show();
    })

    return add
}

new widget({
    id: 'u8YnLc7bw',
    key: 'IK-v_gBD9nDK-rqwI9ZDB3D',
    element: Elements.menuNews
})