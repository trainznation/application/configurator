const path = require('path')
const db = require('electron-db');
const log = require('electron-log')
let slugify = require('slugify')

let pathDatabase = path.join(`${__dirname}`, '..', '..', 'database')
let selectorCategory = document.querySelector('#filter-category')
let selectorState = document.querySelector('#filter-state')
let container = document.querySelector('#table-content')


function loadCategories() {
    db.getAll('category', pathDatabase, (succ, data) => {
        selectorCategory.innerHTML = `<option value="">Tous</option>`;
        Array.from(data).forEach((item) => {
            selectorCategory.innerHTML += `<option value="${item.path}">${item.name}</option>`;
        })
    })
}

function loadAssets() {
    container.innerHTML = `<tr><td colspan="5" class="text-center"><i class="fa fa-spinner fa-spin"></i> Chargement</td></tr>`
    db.getAll('assets', pathDatabase, (succ, data) => {
        if(Array.from(data).length !== 0) {
            container.innerHTML = null
            Array.from(data).forEach((item) => {
                let status =  {
                    'draft': {'title': 'Brouillon', 'class': 'badge-dark', 'icon': 'fa-edit'},
                    'progress': {'title': 'En Cours', 'class': 'badge-warning', 'icon': 'fa-refresh'},
                    'finish': {'title': 'Terminer', 'class': 'badge-info', 'icon': 'fa-check-circle'},
                    'publish': {'title': 'Publier', 'class': 'badge-success', 'icon': 'fa-upload'},
                }
                container.innerHTML += `
            <tr> 
                <td class="text-center align-middle">${item.identifier}</td>
                <td><img src="${path.join(`${__dirname}`, '..', '..', 'job', 'exports', slugify(item.category, '_'), slugify(item.designation, '_'), 'thumbnail.jpg')}" alt="${item.designation}" class="img-fluid"></td>
                <td class="align-middle">${item.designation}<br><strong>KUID: </strong> ${item.kuid}</td>
                <td class="text-center h3 align-middle"><span class="badge ${status[item.state].class}"><i class="fa ${status[item.state].icon}"></i> ${status[item.state].title}</span> </td>
                <td class="text-center align-middle"> 
                    <button class="btn btn-primary btn-sm btnShow" data-id="${item.identifier}" data-toggle="tooltip" title="Voir l'objets"><i class="fa fa-eye"></i> </button>
                    <button class="btn btn-danger btn-sm btnTrash" data-id="${item.identifier}" data-toggle="tooltip" title="Supprimer l'objet"><i class="fa fa-trash"></i> </button>
                </td>
            </tr>
            `
                $('[data-toggle="tooltip"]').tooltip()
            })
        } else {
            container.innerHTML = null
            container.innerHTML += `<tr><td colspan="5" class="text-center"><i class="fa fa-times-circle"></i> Aucunes Données</td></tr>`
        }
    })
}

function bindingCategory() {
    let input = selectorCategory.value
    container.innerHTML = `<tr><td colspan="5" class="text-center"><i class="fa fa-spinner fa-spin"></i> Chargement</td></tr>`
    db.getRows('assets', pathDatabase,{
        'category': input
    }, (succ, result) => {
        if(result.length !== 0) {
            result.forEach((item) => {
                container.innerHTML = null
                container.innerHTML += `
            <tr> 
                <td>${item.identifier}</td>
                <td><img src="${path.join(`${__dirname}`, '..', '..', 'job', 'exports', slugify(item.category, '_'), slugify(item.designation, '_'), 'thumbnail.jpg')}" alt="${item.designation}" class="img-fluid"></td>
                <td>${item.designation}<br><strong>KUID</strong> ${item.kuid}</td>
                <td>${item.state}</td>
                <td></td>
            </tr>
            `
            })
        } else {
            container.innerHTML = null
            container.innerHTML += `<tr><td colspan="5" class="text-center"><i class="fa fa-times-circle"></i> Aucunes Données</td></tr>`
        }
    })

}


function bindingState() {
    let input = selectorState.value
    container.innerHTML = `<tr><td colspan="5" class="text-center"><i class="fa fa-spinner fa-spin"></i> Chargement</td></tr>`
    db.getRows('assets', pathDatabase,{
        'state': input
    }, (succ, result) => {
        if(result.length !== 0) {
            result.forEach((item) => {
                container.innerHTML = null
                container.innerHTML += `
            <tr> 
                <td>${item.id}</td>
                <td><img src="${path.join(`${__dirname}`, '..', '..', 'job', 'exports', slugify(item.category, '_'), slugify(item.designation, '_', 'thumbnail.jpg'))}" alt="${item.designation}" class="img-fluid"></td>
                <td>${item.designation}</td>
                <td>${item.state}</td>
                <td></td>
            </tr>
            `
            })
        } else {
            container.innerHTML = null
            container.innerHTML += `<tr><td colspan="5" class="text-center"><i class="fa fa-times-circle"></i> Aucunes Données</td></tr>`
        }
    })

}

selectorCategory.addEventListener('change', (e) => {
    bindingCategory()
})

selectorState.addEventListener('change', (e) => {
    bindingState()
})

loadCategories()
loadAssets()


