const {sass} = require('laravel-mix')
const mix = require('laravel-mix')
const LiveReloadPlugin = require('webpack-livereload-plugin')

sass('app/assets/sass/styles.scss', 'app/assets/css/styles.css')

mix.webpackConfig({
    plugins: [
        new LiveReloadPlugin()
    ]
});